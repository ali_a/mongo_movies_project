Requirements
============
add network ip to `/etc/mongodb.cfg` on each machine


Start each member of the config server replica set
==================================================
```
mongod --config ./config_server.cfg
```
or
```
mongod --configsvr --replSet config1rs --dbpath ./config_server --bind_ip localhost,192.168.1.XXX --port 5000
```


Connect to one of the config servers
====================================
```
mongo --host 192.168.1.XXX:5000
```

Initiate the replica set
========================
```
rs.initiate(
  {
    _id: "config1rs",
    configsvr: true,
    members: [
      { _id : 0, host : "192.168.1.XXX:5000" },
      { _id : 1, host : "192.168.1.XXX:5000" },
      { _id : 2, host : "192.168.1.XXX:5000" }
    ]
  }
)
```


Start each member of the shard replica set
==========================================
```
mongod --config ./shard_server.cfg
```
or
```
mongod --shardsvr --replSet shard1rs --dbpath ./shard_server --bind_ip localhost,192.168.1.XXX --port 6000
```


Connect to one member of the shard replica set
==============================================
```
mongo --host 192.168.1.XXX:6000
```


Initiate the replica set
========================
```
rs.initiate(
  {
    _id : "shard1rs",
    members: [
      { _id : 0, host : "192.168.1.XXX:6000" },
      { _id : 1, host : "192.168.1.XXX:6000" },
      { _id : 2, host : "192.168.1.XXX:6000" }
    ]
  }
)
```


Start a mongos for the Sharded Cluster
======================================
```
mongos --config ./mongos.cfg
```
or
```
mongos --configdb shard1rs/192.168.1.XXX:6000,192.168.1.XXX:6000,192.168.1.XXX:6000 --bind_ip localhost,192.168.1.XXX --port 7000
```

Connect to the Sharded Cluster
==============================
```
mongo --host 192.168.1.XXX:7000
```

Add Shards to the Cluster
=========================
```
sh.addShard( "shard1rs/192.168.1.XXX:6000,192.168.1.XXX:6000,192.168.1.XXX:6000")
```
Repeat these steps until the cluster includes all desired shards.


Enable Sharding for a Database
==============================
```
mongo --host 192.168.1.XXX:7000
sh.enableSharding("<database>")
```


Shard a Collection
==================
```
sh.shardCollection("<database>.<collection>", { <shard key field> : "hashed" } )
```
or
```
sh.shardCollection("<database>.<collection>", { <shard key field> : 1, ... } )
```