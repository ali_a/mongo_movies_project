#!/usr/bin/env python3
import random
import json

# usernames
users_file = '/home/ali/Desktop/MONGO/users/users.json'
with open(users_file, 'r') as f:
    file_data = f.read()
users = [json.loads(j) for j in file_data.split('\n')]
usernames = [username['username'] for username in users]

comments = [
    'that is true.',
    'I agree.',
    'lol.',
    'haha.',
    'agreed.',
    'I don\'t think so.',
    'totally.',
    'exactly.',
    'precisely.',
    'oh well.',
    'I don\'t think so.',
    'why would you say that?',
    'I can not believe you.',
    'wait, what?',
    'now we are talking.',
    'poor post.',
    'really?',
    'oh well.',
    'true.',
    'not true.',
    'seriously?',
    'jesus.',
    'stop posting.',
    'who are you again?',
    'how dare you?',
    'well well.',
    'you did not watched it.',
    'carefull notes.',
    'get out of here lol.',
    'you mad or something?',
    'blocked.',
    'just try to watch it again.',
    'snakes have legs.',
    'not funny.',
    'haha funny.',
    'you are out of your mind!',
    'you think before you write?',
    'well written.',
    'well said.',
    'good job writing this trash.',
    'poor guy.',
    'cast was OK actually.',
    'I would say NOOOO',
    'you think so?',
    'you know the director, right?',
    'let\'s discuse on IRC.',
    'well let\'s talk on discord',
    'where do you stand on cast?',
    'speak ENGLISH!',
    'you are not allowed to say such things.',
    'reported.',
    'are you O.K.?',
    'imagine... dragons.',
    'to watch or not to watch.',
    'you don\'t have to post dude.',
    'dear lord.',
    'yes that\'s it.',
    'you are hopeless.',
    'I like your point of view.',
    'you bet.',
    'correct.',
    'that is correct,',
    'truth is you are WRONG.',
    'missleading.',
    'not correct.',
    'not exactly.',
    'lord is pleased by you.',
    'parden me?',
    'how come?',
    'you don\'t say!',
    'Ummmm.',
    'Are you robot?',
    'I feel you.',
    'I see.',
    'I get you.',
    'I understand.',
    'like dombeldor?',
    'Aha.',
    'nothing is perfect.',
    'I\'m sold.',
    'say no more.',
    'anyone bought that?',
    'no one cares.',
    'shocking.'
    ]

result = ''
pure_comments = ''
for comment_id in range(2_000_000):
    username = random.choice(usernames)
    object_id_of_post = random.choice(range(100_000))
    comment = ' '.join(random.choices(comments, k=3))
    json_element = '{"username":"%s", "post_id":%s, "comment":"%s"}\n' %\
                    (username, object_id_of_post, comment)
    result += json_element
    pure_comments += f'{comment}\n'
    
with open('comments.json', 'w') as f:
    f.write(result)

with open('pure_comments.txt', 'w') as f:
    f.write(pure_comments)