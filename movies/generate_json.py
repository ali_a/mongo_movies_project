#!/usr/bin/env python3
from os.path import join
import json
import pandas

dataset_direstory = '/home/ali/Projects/MONGO/datasets/kaggle 5000'
movies = join(dataset_direstory, 'movies.csv')
movie_credits = join(dataset_direstory, 'credits.csv')

movies_dataframe = pandas.read_csv(movies)
titles = movies_dataframe['title']
dates = movies_dataframe['release_date']
dates = dates.apply(lambda x: str(x)[:4])

credits_dataframe = pandas.read_csv(movie_credits)
cast = credits_dataframe['cast']
crew = credits_dataframe['crew']

cast = cast.apply(json.loads)
crew = crew.apply(json.loads)

result = pandas.concat([titles, dates, cast, crew], axis=1)
result['_id'] = result.index
result.to_json('movies.json', orient='records')
result.to_json('lined_movies.json', orient='records', lines=True)
result.to_csv('result.csv', index=False)