#!/usr/bin/env python3
import random
import json

# titles
movies_json_file = '/home/ali/Desktop/MONGO/movies/movies.json'
with open(movies_json_file, 'r') as f:
    file_data = f.read()
json_data = json.loads(file_data)
titles = [title['title'] for title in json_data]

# bodies
sentences_file = '/home/ali/Desktop/MONGO/datasets/posts/sentiment labelled sentences/imdb_labelled.txt'
with open(sentences_file, 'r') as f:
    file_data = f.read()
sentences = file_data.split('\n')
bodies = [sentence.split('\t')[0].strip() for sentence in sentences]
bodies = [body.replace('"', "'") for body in bodies]

# usernames
users_file = '/home/ali/Desktop/MONGO/users/users.json'
with open(users_file, 'r') as f:
    file_data = f.read()
users = [json.loads(j) for j in file_data.split('\n')]
usernames = [username['username'] for username in users]

# generate posts
result = ''
for post_id in range(100_000):
    post_title = random.choice(titles)
    post_body = random.choice(bodies)
    post_username = random.choice(usernames)
    json_element = '{"_id":%s, "title":"%s", "username":"%s", "body":"%s"}\n' %\
                    (post_id, post_title, post_username, post_body)
    result += json_element

with open('posts.json', 'w') as f:
    f.write(result)
