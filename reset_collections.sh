#!/bin/bash
splitter='================================================================================'

function header(){
    echo
    echo $splitter
    echo "$1"
    echo $splitter
}

header 'Dropping Database'
mongo project --eval 'db.dropDatabase()'

header 'Importing movie collection'
mongoimport --db project --collection movies --jsonArray --file ./movies/movies.json

header 'Importing user collection'
mongoimport --db project --collection users --file ./users/users.json

header 'Creating Index on body of posts'
mongo project --eval 'db.posts.createIndex({body:"text"})'

header 'Importing post collecton'
mongoimport --db project --collection posts --file ./posts/posts.json

header 'Importng comment collection'
mongoimport --db project --collection comments --file ./comments/comments.json

echo
echo $splitter
echo 'done.'
sleep 999